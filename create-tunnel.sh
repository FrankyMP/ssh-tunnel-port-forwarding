#!/bin/bash

# export .env as variable
export $(grep -v '^#' .env | xargs -d '\n')

##if pgrep -f "ssh.*-R" > /dev/null; then # find process with 'ssh -R' command
if [ $(curl --max-time 15 -sIXGET $remoteURL | grep -E "HTTP\/" | awk '{print $2}') -eq 200 ]; then # Check web status code
    exit 0; # exit if process exist
else
    kill -9 $(pgrep -f "ssh.*-R $remotePort") # 
    ssh -i $pemFile -R $remotePort:$localIP:$localPort -N -f $remoteUser@$remoteIP; # create tunnel if process not exist
fi